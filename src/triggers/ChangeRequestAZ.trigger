trigger ChangeRequestAZ on AxtriaSalesIQTM__Change_Request__c (after delete, after insert, after update) {

    
    if(trigger.isInsert)
    {
        ChnageRequestTriggerHandlerAZ.createCRCallPlan(trigger.new);
    }
    else if(trigger.isUpdate)
    {
        ChnageRequestTriggerHandlerAZ.updatePositionAccountCallPlan(trigger.new);
    }

}
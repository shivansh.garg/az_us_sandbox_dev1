<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Stores Approver Details related to the Role referencing the MBO_Plan</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>AxtriaSalesIQTM__Action__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Action</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>No Action</fullName>
                    <default>false</default>
                    <label>No Action</label>
                </value>
                <value>
                    <fullName>Self Rating</fullName>
                    <default>false</default>
                    <label>Self Rating</label>
                </value>
                <value>
                    <fullName>Ranking</fullName>
                    <default>false</default>
                    <label>Ranking</label>
                </value>
                <value>
                    <fullName>Approval</fullName>
                    <default>false</default>
                    <label>Approval</label>
                </value>
                <value>
                    <fullName>Ranking and Approval</fullName>
                    <default>false</default>
                    <label>Ranking and Approval</label>
                </value>
                <value>
                    <fullName>Ranking and Submit</fullName>
                    <default>false</default>
                    <label>Ranking and Submit</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>AxtriaSalesIQTM__Cycle__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Cycle</label>
        <referenceTo>AxtriaSalesIQTM__MBO_Cycle__c</referenceTo>
        <relationshipLabel>Approver Details</relationshipLabel>
        <relationshipName>Approver_Details</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>AxtriaSalesIQTM__End_Date__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>End Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>AxtriaSalesIQTM__Is_Active__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Is Active</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>AxtriaSalesIQTM__Plan__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Plan</label>
        <referenceTo>AxtriaSalesIQTM__MBO_Plan__c</referenceTo>
        <relationshipName>MBO_Approver_Detail</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>AxtriaSalesIQTM__Role__c</fullName>
        <deprecated>false</deprecated>
        <description>Stores Approval&apos;s Profiles</description>
        <externalId>false</externalId>
        <label>Role</label>
        <length>80</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>AxtriaSalesIQTM__Sequence__c</fullName>
        <deprecated>false</deprecated>
        <description>Stores Approval sequence</description>
        <externalId>false</externalId>
        <label>Sequence</label>
        <precision>3</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>AxtriaSalesIQTM__Start_Date__c</fullName>
        <deprecated>false</deprecated>
        <description>Start Date of the Plan</description>
        <externalId>false</externalId>
        <label>Start Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>AxtriaSalesIQTM__To_Publish__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>To Publish</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <label>Approver Detail</label>
    <listViews>
        <fullName>AxtriaSalesIQTM__All</fullName>
        <columns>NAME</columns>
        <columns>AxtriaSalesIQTM__Plan__c</columns>
        <columns>AxtriaSalesIQTM__Cycle__c</columns>
        <columns>AxtriaSalesIQTM__Is_Active__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <listViews>
        <fullName>AxtriaSalesIQTM__All1</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <displayFormat>A-{0000}</displayFormat>
        <label>Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Approver Details</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
    <visibility>Public</visibility>
</CustomObject>

public with sharing class ChangeRequestTriggerHandlerAZ {
public static void createCRCallPlan(List<AxtriaSalesIQTM__Change_Request__c> changeRequestList){    
      AxtriaSalesIQTM__Change_Request__c CR;
        for(AxtriaSalesIQTM__Change_Request__c objCR: changeRequestList ) CR = objCR;
        ID crID = CR.ID;
        List<String> accChanged = CR.AxtriaSalesIQTM__Account_Moved_Id__c.split(',');        
        ID destPos = CR.AxtriaSalesIQTM__Destination_Position__c;
    id teamInstanceID = CR.AxtriaSalesIQTM__Team_Instance_ID__c;  
    
    List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> allChangedRec = new list <AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
    
        string soql ='select AxtriaSalesIQTM__Account__r.ID,AxtriaSalesIQTM__Account__r.AccountNumber,AxtriaSalesIQTM__Account__r.Name,AxtriaSalesIQTM__Metric3__c,AxtriaSalesIQTM__Metric3_Approved__c,AxtriaSalesIQTM__Metric3_Updated__c,AxtriaSalesIQTM__Picklist1_Updated__c,AxtriaSalesIQTM__Picklist1_Segment_Approved__c,AxtriaSalesIQTM__Picklist3_Updated__c,AxtriaSalesIQTM__Picklist3_Segment_Approved__c,AxtriaSalesIQTM__Segment7__c,AxtriaSalesIQTM__Picklist2_Updated__c,AxtriaSalesIQTM__Picklist2_Segment_Approved__c,AxtriaSalesIQTM__Change_Status__c,AxtriaSalesIQTM__isIncludedCallPlan__c,AxtriaSalesIQTM__Segment2__c,AxtriaSalesIQTM__Segment8__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Account__r.AccountNumber in :accChanged and AxtriaSalesIQTM__Position__c =:destPos and AxtriaSalesIQTM__Team_Instance__c =:teamInstanceID';
        
            allChangedRec = Database.query(soql);
       
        List<AxtriaSalesIQTM__CR_Call_Plan__c> crCallPlanRequests = new List<AxtriaSalesIQTM__CR_Call_Plan__c>();        
        AxtriaSalesIQTM__CR_Call_Plan__c tempCRCallPlan;         
        
        for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp : allChangedRec) 
        {
            tempCRCallPlan = new AxtriaSalesIQTM__CR_Call_Plan__c();
            tempCRCallPlan.AxtriaSalesIQTM__Change_Request_ID__c = crID;
            tempCRCallPlan.name = 'CR-' + SalesIQUtility.GenerateRandomNumber();
            if(pacp.AxtriaSalesIQTM__Picklist1_Segment_Approved__c != (pacp.AxtriaSalesIQTM__Picklist1_Updated__c)){
             tempCRCallPlan.AxtriaSalesIQTM__Rep_Goal_Before__c = (pacp.AxtriaSalesIQTM__Picklist1_Segment_Approved__c);
              tempCRCallPlan.AxtriaSalesIQTM__Rep_Goal_After__c = pacp.AxtriaSalesIQTM__Picklist1_Updated__c;
              tempCRCallPlan.AxtriaSalesIQTM__Original_Reason_Code__c = pacp.AxtriaSalesIQTM__Picklist3_Segment_Approved__c;
              tempCRCallPlan.AxtriaSalesIQTM__Updated_Reason_Code__c = pacp.AxtriaSalesIQTM__Picklist3_Updated__c;
             tempCRCallPlan.AxtriaSalesIQTM__isIncludedCallPlan__c = pacp.AxtriaSalesIQTM__isIncludedCallPlan__c;
              
          }
          
         
          tempCRCallPlan.AxtriaSalesIQTM__Status__c = SalesIQGlobalConstants.REQUEST_STATUS_PENDING;
          tempCRCallPlan.AxtriaSalesIQTM__Account__c = pacp.AxtriaSalesIQTM__Account__r.ID;
          crCallPlanRequests.add(tempCRCallPlan);
        }
        
            
        if(crCallPlanRequests!=null && crCallPlanRequests.size() > 0){
          
          
              insert crCallPlanRequests;
          
        }
        
        if(CR.AxtriaSalesIQTM__Status__c == SalesIQGlobalConstants.REQUEST_STATUS_APPROVED){
          updatePositionAccountCallPlan(changeRequestList);
        }
    }
    
    /**
  * This method updates Position Account Call Plan after Call Plan Request is Approved / Rejected  
  * @param List<AxtriaSalesIQTM__Change_Request__c> list of all Change Requests
  */    
    public static void updatePositionAccountCallPlan(List<AxtriaSalesIQTM__Change_Request__c> changeRequestList)
    {
        AxtriaSalesIQTM__Change_Request__c CR;
        for(AxtriaSalesIQTM__Change_Request__c objCR: changeRequestList ) CR = objCR;
        if(CR != null){
          if(CR.AxtriaSalesIQTM__Status__c == SalesIQGlobalConstants.REQUEST_STATUS_APPROVED || CR.AxtriaSalesIQTM__Status__c == SalesIQGlobalConstants.REQUEST_STATUS_REJECTED || CR.AxtriaSalesIQTM__Status__c ==SalesIQGlobalConstants.REQUEST_STATUS_RECALLED || CR.AxtriaSalesIQTM__Status__c == SalesIQGlobalConstants.REQUEST_STATUS_CANCELLED) {
          ID crID = CR.ID;
             List<String> accChanged = CR.AxtriaSalesIQTM__Account_Moved_Id__c.split(',');
             Map<String,AxtriaSalesIQTM__CR_Call_Plan__c> crCallPlanMap = null;            
           
             string soql= 'select AxtriaSalesIQTM__Account__c,AxtriaSalesIQTM__Account__r.AccountNumber,AxtriaSalesIQTM__isIncludedCallPlan__c,AxtriaSalesIQTM__Updated_Reason_Code__c   ,AxtriaSalesIQTM__Rep_Goal_After__c from AxtriaSalesIQTM__CR_Call_Plan__c where AxtriaSalesIQTM__Change_Request_ID__c= :crID';
            
            List<AxtriaSalesIQTM__CR_Call_Plan__c> allCRrecs = new list <AxtriaSalesIQTM__CR_Call_Plan__c>();
          
              allCRrecs = Database.query(soql);
          
            
            if(allCRrecs != null && allCRrecs.size() > 0){
              crCallPlanMap = new Map<String,AxtriaSalesIQTM__CR_Call_Plan__c>();
                for(AxtriaSalesIQTM__CR_Call_Plan__c crcp : allCRrecs){
                  crcp.AxtriaSalesIQTM__Status__c = CR.AxtriaSalesIQTM__Status__c;
                    crCallPlanMap.put(crcp.AxtriaSalesIQTM__Account__r.AccountNumber,crcp);  
                }
            
              ID destPos = CR.AxtriaSalesIQTM__Destination_Position__c;
        ID teamInstanceID = CR.AxtriaSalesIQTM__Team_Instance_ID__c;
        List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> posAccCallPlanRecs = new list <AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
            soql ='select AxtriaSalesIQTM__Account__r.ID,AxtriaSalesIQTM__Account__r.AccountNumber,AxtriaSalesIQTM__Account__r.Name,AxtriaSalesIQTM__Metric3__c,AxtriaSalesIQTM__Metric3_Approved__c,AxtriaSalesIQTM__Metric3_Updated__c,AxtriaSalesIQTM__Picklist1_Updated__c,AxtriaSalesIQTM__Picklist1_Segment__c, AxtriaSalesIQTM__Picklist1_Segment_Approved__c,AxtriaSalesIQTM__Picklist3_Updated__c,AxtriaSalesIQTM__Picklist3_Segment_Approved__c,AxtriaSalesIQTM__Picklist2_Updated__c,AxtriaSalesIQTM__Picklist2_Segment_Approved__c,AxtriaSalesIQTM__Picklist2_Segment__c,AxtriaSalesIQTM__Change_Status__c,AxtriaSalesIQTM__isIncludedCallPlan__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Account__r.AccountNumber in :accChanged and AxtriaSalesIQTM__Position__c =:destPos and AxtriaSalesIQTM__Team_Instance__c =:teamInstanceID';
               
            
                  posAccCallPlanRecs = Database.query(soql);
             
              boolean flag = true; // Incase any other changes have been made
              
              list<AxtriaSalesIQTM__Position_Account_Call_Plan__c> lstToUpdate = new list<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
              for(AxtriaSalesIQTM__Position_Account_Call_Plan__c pacp : posAccCallPlanRecs) {
                if(crCallPlanMap.containsKey(pacp.AxtriaSalesIQTM__Account__r.AccountNumber)){
                    AxtriaSalesIQTM__CR_Call_Plan__c crcp = crCallPlanMap.get(pacp.AxtriaSalesIQTM__Account__r.AccountNumber);
                      
                      if(CR.AxtriaSalesIQTM__Status__c == SalesIQGlobalConstants.REQUEST_STATUS_APPROVED ){
                        pacp.AxtriaSalesIQTM__Picklist3_Segment_Approved__c = pacp.AxtriaSalesIQTM__Picklist3_Updated__c ;
                          pacp.AxtriaSalesIQTM__Picklist1_Segment_Approved__c = pacp.AxtriaSalesIQTM__Picklist1_Updated__c ;
                          pacp.AxtriaSalesIQTM__Change_Status__c =  SalesIQGlobalConstants.REQUEST_STATUS_APPROVED;
                          
                          pacp.AxtriaSalesIQTM__Metric3_Approved__c = pacp.AxtriaSalesIQTM__Metric3_Updated__c;
                          pacp.AxtriaSalesIQTM__Picklist2_Segment_Approved__c = pacp.AxtriaSalesIQTM__Picklist2_Updated__c;
                      }
                      else if(CR.AxtriaSalesIQTM__Status__c ==  SalesIQGlobalConstants.REQUEST_STATUS_REJECTED || CR.AxtriaSalesIQTM__Status__c ==  SalesIQGlobalConstants.REQUEST_STATUS_RECALLED || CR.AxtriaSalesIQTM__Status__c == SalesIQGlobalConstants.REQUEST_STATUS_CANCELLED){
                          
	                        	pacp.AxtriaSalesIQTM__Change_Status__c = SalesIQGlobalConstants.REQUEST_STATUS_SUBMISSION_PENDING;
	                                              
                          }
                      else{ flag = false; }
                      lstToUpdate.add(pacp);
                  }
              }
              try{
                  
                    update lstToUpdate;
                  
                       update allCRrecs;
                               
              }
              catch(Exception e){              
                  system.debug('exception id updatePositionAccountCallPlan ' + e.getMessage());
              }
          }
            updateMetricSummaryTable(CR.AxtriaSalesIQTM__Status__c , CR.AxtriaSalesIQTM__Destination_Position__c,CR.AxtriaSalesIQTM__Team_Instance_ID__c );            
        }
        }
        
    }
    
    /**
  * This method updates metric Summary Table after call plan request is Approved or Rejected  
  * @param String :- Request is approved or rejected
  * @param string Position ID
  */    
     
    public static void updateMetricSummaryTable(string type , string posID , string teamInstance){
      Set<ID> allCIMids = new Set<ID>();
      List<AxtriaSalesIQTM__CIM_Config__c> cimConfigRecs = new list <AxtriaSalesIQTM__CIM_Config__c>();
    string cimQuery = 'select id from AxtriaSalesIQTM__CIM_Config__c where AxtriaSalesIQTM__Team_Instance__c = :teamInstance and AxtriaSalesIQTM__Change_Request_Type__r.AxtriaSalesIQTM__CR_Type_Name__c = \'Call_Plan_Change\'';
    
         cimConfigRecs = Database.query(cimQuery);
    
    for(AxtriaSalesIQTM__CIM_Config__c ccm : cimConfigRecs){
      allCIMids.add(ccm.ID);  
    }
        
        list<AxtriaSalesIQTM__CIM_Position_Metric_Summary__c> cimPosMetricRecs = new list <AxtriaSalesIQTM__CIM_Position_Metric_Summary__c>();        
        string fetchAllPosMetric = 'select AxtriaSalesIQTM__Proposed__c, AxtriaSalesIQTM__Approved__c,AxtriaSalesIQTM__CIM_Config__r.Name from AxtriaSalesIQTM__CIM_Position_Metric_Summary__c where AxtriaSalesIQTM__CIM_Config__c in :allCIMids and AxtriaSalesIQTM__Position_Team_Instance__r.AxtriaSalesIQTM__Position_ID__c = :posID ';
     
      cimPosMetricRecs = Database.query(fetchAllPosMetric);
        
      if(type == SalesIQGlobalConstants.REQUEST_STATUS_APPROVED){    
            for(AxtriaSalesIQTM__CIM_Position_Metric_Summary__c cpms : cimPosMetricRecs)
              cpms.AxtriaSalesIQTM__Approved__c = cpms.AxtriaSalesIQTM__Proposed__c;
      }else if(type == SalesIQGlobalConstants.REQUEST_STATUS_REJECTED || type ==  SalesIQGlobalConstants.REQUEST_STATUS_RECALLED  || type ==  SalesIQGlobalConstants.REQUEST_STATUS_CANCELLED   ){
          for(AxtriaSalesIQTM__CIM_Position_Metric_Summary__c cpms : cimPosMetricRecs){
          	
          	
          }	
        }
      update cimPosMetricRecs;   
  }

}
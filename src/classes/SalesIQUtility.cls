public with sharing class SalesIQUtility {
    
    public static list<AxtriaSalesIQTM__User_Access_Permission__c> getUserAccessPermistion(string UserID){
        
        list<AxtriaSalesIQTM__User_Access_Permission__c> accessRecs = new List<AxtriaSalesIQTM__User_Access_Permission__c>();
        accessRecs = [select AxtriaSalesIQTM__Position__c,AxtriaSalesIQTM__Position__r.Name,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Position_Type__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Parent_Position__c, AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__Team_Instance__r.Name from AxtriaSalesIQTM__User_Access_Permission__c where AxtriaSalesIQTM__User__c = :UserID and AxtriaSalesIQTM__Is_Active__c = True order by AxtriaSalesIQTM__Position__r.Name];
       
        return accessRecs;
    }

  public static id getselctedRecordType(string ObjectName, string Type){
        Map<String, Schema.SObjectType> m  = Schema.getGlobalDescribe() ;
        Schema.SObjectType s = m.get(ObjectName) ;
       
        Schema.DescribeSObjectResult cfrSchema = s.getDescribe() ;
        Map<String,Schema.RecordTypeInfo> RecordTypeInfo = cfrSchema.getRecordTypeInfosByName();       
        id recordType =  RecordTypeInfo.get(Type).getRecordTypeId();      
        return recordType;
    }
    public static string GenerateRandomNumber(){
    	Double rnd =  Math.random();		
		string randomstring =  string.valueof(Math.random());		
		string str  = randomstring.replace('.','2');
		return str.subString(0,5);
	}
    
}
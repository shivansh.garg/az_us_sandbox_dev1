public with sharing class CIMPositionMatrixSummaryUpdateCtlr {
    public list<AxtriaSalesIQTM__CIM_Config__c> listCIMconfig{get; set;}
    public string teamInstanceID{get; set;}
    public string cimConfigId{get; set;}
    public list<SelectOption> teamInstanceOptions{get;set;}
    public list<SelectOption> cimConfigOptions{get; set;}
    public string theQuery{get; set;}
    public string aggregationAttributeAPIName{get; set;}
    public list<AxtriaSalesIQTM__CIM_Position_Metric_Summary__c> updateListPostionMetric{get; set;}
    public integer countRecords {get;set;}
    
    public CIMPositionMatrixSummaryUpdateCtlr(){     
        theQuery = '';
        teamInstanceOptions = new List<SelectOption>();
        cimConfigOptions = new list<SelectOption>();
        list<AxtriaSalesIQTM__Team_Instance__c> teamInstances = [select id, name from AxtriaSalesIQTM__Team_Instance__c limit 100];
        teamInstanceOptions.add(new SelectOption('','--None--'));
        for(AxtriaSalesIQTM__Team_Instance__c objTeamInstance: teamInstances){
            teamInstanceOptions.add(new SelectOption(objTeamInstance.id,objTeamInstance.name));
        }  
    }
    
    public void cimconfigrefresh(){
        cimConfigOptions = new list<SelectOption>();
        listCIMconfig = [select id, name, AxtriaSalesIQTM__Object_Name__c, AxtriaSalesIQTM__Attribute_API_Name__c, AxtriaSalesIQTM__Aggregation_Type__c, AxtriaSalesIQTM__Aggregation_Object_Name__c, AxtriaSalesIQTM__Aggregation_Attribute_API_Name__c, AxtriaSalesIQTM__Aggregation_Condition_Attribute_API_Name__c, AxtriaSalesIQTM__Aggregation_Condition_Attribute_Value__c  from AxtriaSalesIQTM__CIM_Config__c where AxtriaSalesIQTM__team_instance__c =:teamInstanceID and AxtriaSalesIQTM__Enable__c=true limit 1000];
        cimConfigOptions.add(new SelectOption('','--None--'));
        for(AxtriaSalesIQTM__CIM_Config__c objCIMConfig : listCIMconfig){
            cimConfigOptions.add(new SelectOption(objCIMConfig.id,objCIMConfig.name));
        }
    }
    
    public void updatePostionMatrxiSummary(){
        displayQuery();
        if(updateListPostionMetric.size() > 0){
            insert updateListPostionMetric;
        }
        theQuery = 'The values have been inserted, thanks!';
    }
    
    public void displayQuery(){
        updateListPostionMetric= new list<AxtriaSalesIQTM__CIM_Position_Metric_Summary__c>();
        //listCIMconfig = [select id, Object_Name__c, Attribute_API_Name__c, Aggregation_Type__c, Aggregation_Object_Name__c, Aggregation_Attribute_API_Name__c, Aggregation_Condition_Attribute_API_Name__c, Aggregation_Condition_Attribute_Value__c  from CIM_Config__c where team_instance__c=:teaminstanceID and Aggregation_Object_Name__c='Position_Account_Call_Plan__c'];
        listCIMconfig = [select id, name, AxtriaSalesIQTM__Object_Name__c, AxtriaSalesIQTM__Attribute_API_Name__c, AxtriaSalesIQTM__Aggregation_Type__c, AxtriaSalesIQTM__Aggregation_Object_Name__c, AxtriaSalesIQTM__Aggregation_Attribute_API_Name__c, AxtriaSalesIQTM__Aggregation_Condition_Attribute_API_Name__c, AxtriaSalesIQTM__Aggregation_Condition_Attribute_Value__c  from AxtriaSalesIQTM__CIM_Config__c where id=:cimConfigId and AxtriaSalesIQTM__team_instance__c =:teamInstanceID];
        
        for(AxtriaSalesIQTM__CIM_Config__c obj : listCIMconfig){
            string objectName = obj.AxtriaSalesIQTM__Object_Name__c;
            string attributeAPIname = obj.AxtriaSalesIQTM__Attribute_API_Name__c;
            string aggregationType = obj.AxtriaSalesIQTM__Aggregation_Type__c;
            string aggregationObjectName = obj.AxtriaSalesIQTM__Aggregation_Object_Name__c;
            aggregationAttributeAPIName = obj.AxtriaSalesIQTM__Aggregation_Attribute_API_Name__c;
            string aggregationConditionAttributeAPIName = obj.AxtriaSalesIQTM__Aggregation_Condition_Attribute_API_Name__c;
            //string aggregationConditionAttributeValue = obj.AxtriaSalesIQTM__Aggregation_Condition_Attribute_Value__c;
            
            string objectToBeQueried;
            if(aggregationAttributeAPIName!= null && aggregationObjectName!=null ){
                
                if(objectName != aggregationObjectName){
                    objectToBeQueried =  objectName.removeEnd('c');
                    objectToBeQueried = objectToBeQueried + 'r.'+attributeAPIname;
                    
                    
                    if(aggregationConditionAttributeAPIName != null){
                        theQuery = 'select '+aggregationType+'(' + objectToBeQueried + '), ' + aggregationAttributeAPIName + ' from ' +
                        aggregationObjectName + ' where AxtriaSalesIQTM__team_instance__c =:teamInstanceID and ' + aggregationConditionAttributeAPIName + ' Group by '+  aggregationAttributeAPIName;
                    }
                    else{
                        theQuery = 'select '+aggregationType+'(' + objectToBeQueried + '), ' + aggregationAttributeAPIName + ' from ' +
                        aggregationObjectName + ' where AxtriaSalesIQTM__team_instance__c =:teamInstanceID '+ ' group by '+  aggregationAttributeAPIName;
                    }
                    
                    system.debug('the Query '+ theQuery);
                }
                else{
                    objectToBeQueried = attributeAPIname;
                    if(aggregationConditionAttributeAPIName != null){
                        theQuery = 'select '+aggregationType+'(' + objectToBeQueried + '), ' + aggregationAttributeAPIName + ' from ' +
                        aggregationObjectName + ' where AxtriaSalesIQTM__team_instance__c =:teamInstanceID and ' + aggregationConditionAttributeAPIName + ' Group by '+  aggregationAttributeAPIName;
                    }
                    else{
                        theQuery = 'select '+aggregationType+'(' + objectToBeQueried + '), ' + aggregationAttributeAPIName + ' from ' +
                        aggregationObjectName + ' where AxtriaSalesIQTM__team_instance__c =:teamInstanceID '+ ' Group by '+  aggregationAttributeAPIName;
                    }      
                    system.debug('======= '+ aggregationConditionAttributeAPIName);
                    system.debug('the Query '+ theQuery);
                    
                }
                
                AggregateResult[] updateList = Database.query(theQuery);
                system.debug('UpdateList '+ updateList);
                for(AggregateResult  objCIMConfig : updateList){
                    //aggregationObjectName objCimFromSobject = (aggregationObjectName)objCIMConfig;
                    String originalCalls = String.valueOF(objCIMConfig.get('expr0'));
                    
                    AxtriaSalesIQTM__CIM_Position_Metric_Summary__c objCIMPositionMetric = new AxtriaSalesIQTM__CIM_Position_Metric_Summary__c(AxtriaSalesIQTM__CIM_Config__c = cimConfigId, AxtriaSalesIQTM__Original__c = originalCalls, AxtriaSalesIQTM__Team_Instance__c = teamInstanceID, AxtriaSalesIQTM__Proposed__c = originalCalls, AxtriaSalesIQTM__Approved__c = originalCalls, AxtriaSalesIQTM__Position_Team_Instance__c = String.valueOF(objCIMConfig.get(aggregationAttributeAPIName)));
                    updateListPostionMetric.add(objCIMPositionMetric);
                    
                }
                
                
                set<string> givenPosTeamIns = new set<string>();
                for(AxtriaSalesIQTM__CIM_Position_Metric_Summary__c updPosMetr : updateListPostionMetric){
                    givenPosTeamIns.add(updPosMetr.AxtriaSalesIQTM__Position_Team_Instance__c);
                }
                
                string soqlQuery = 'select id, name from AxtriaSalesIQTM__position_team_instance__c where AxtriaSalesIQTM__Team_Instance_ID__c=:teamInstanceID';
                list<AxtriaSalesIQTM__Position_team_instance__c> listPosTeamIns = Database.query(soqlQuery);
                
                set<string> allPosTeamIns = new set<string>();
                for(AxtriaSalesIQTM__Position_team_instance__c posTeamInsObj : listPosTeamIns){
                    allPosTeamIns.add(posTeamInsObj.id);
                }
                 
                allPosTeamIns.removeAll(givenPosTeamIns);
                
                for(string str : allPosTeamIns){
                    AxtriaSalesIQTM__CIM_Position_Metric_Summary__c objCIMPositionMetric = new AxtriaSalesIQTM__CIM_Position_Metric_Summary__c(AxtriaSalesIQTM__CIM_Config__c = cimConfigId, AxtriaSalesIQTM__Original__c = '0', AxtriaSalesIQTM__Team_Instance__c = teamInstanceID, AxtriaSalesIQTM__Proposed__c = '0', AxtriaSalesIQTM__Approved__c = '0', AxtriaSalesIQTM__Position_Team_Instance__c = str);
                    updateListPostionMetric.add(objCIMPositionMetric);
                }

                countRecords = updateListPostionMetric.size();
                set<id> positionTeamInstance = new set<id>();
                set<id> cimConfigSet = new set<id>();
                for(AxtriaSalesIQTM__CIM_Position_Metric_Summary__c objCIMpos : updateListPostionMetric){
                    positionTeamInstance.add(objCIMpos.AxtriaSalesIQTM__Position_Team_Instance__c);
                    cimConfigSet.add(objCIMpos.AxtriaSalesIQTM__CIM_Config__c);
                }
                
                list<AxtriaSalesIQTM__CIM_Position_Metric_Summary__c> cimPositionMetriclist = [select id from AxtriaSalesIQTM__CIM_Position_Metric_Summary__c where AxtriaSalesIQTM__CIM_Config__c in:cimConfigSet and AxtriaSalesIQTM__Position_Team_Instance__c in:positionTeamInstance];
                if(cimPositionMetriclist.size() > 0){
                    theQuery = 'The configuration is present in CIM_Position_Metric_Summary table already!';
                }
                system.debug('updateListPostionMetric '+updateListPostionMetric);       
            }   
        }
    }
      
}
public with sharing class RedirectToReport {
  
  public string loggedInPosition {get;set;}
  public string loggedInPositionclientcode {get;set;}
  public String gProfileName{get;set;}
  public String reportname{get;set;}
  
  public RedirectToReport()
  {
    
  }
  
  public PageReference openReport()
  {
  	gProfileName = [Select Id,Name from Profile where Id=:UserInfo.getProfileId()].Name;
    AxtriaSalesIQTM__User_Access_Permission__c uap = [select id,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Position__r.Name from AxtriaSalesIQTM__User_Access_Permission__c where AxtriaSalesIQTM__User__c = :Userinfo.getUserId() LIMIT 1];  
    loggedInPosition = uap.AxtriaSalesIQTM__Position__r.Name;
    loggedInPositionclientcode = uap.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c;
    
    if(reportname=='Call Plan Summary'){
	    List<Report> rp;// = [SELECT Name FROM Report where Name='Call Plan View Report'];
	    
	    /*if(gProfileName =='Rep')
	    {
	    	rp = [SELECT Name FROM Report where Name='Call Plan View Report'];
	    }
	    else */
	    if(gProfileName =='DM')
	    {
	    	rp = [SELECT Name FROM Report where Name='Call Plan View Report DM'];
	    }
	    else if(gProfileName =='RM')
	    {
	    	rp = [SELECT Name FROM Report where Name='Call Plan View Report RM'];
	    }
	    else if(gProfileName =='HO')
	    {
	    	rp = [SELECT Name FROM Report where Name='Call Plan View Report HO'];
	    }
	    else
	    {
	    	rp = [SELECT Name FROM Report where Name='Call Plan View Report'];
	    }
	    
	    if(rp.size()>0) 
	    {
	      PageReference pg = new PageReference ('/'+rp[0].Id+'?pv0='+loggedInPositionclientcode);
	      
	      pg.setRedirect(true);
	      
	      return pg;      
	    }
    }
    else if(reportname=='Call Plan Summary Added/Dropped'){
	    List<Report> rp;// = [SELECT Name FROM Report where Name='Call Plan View Report Added/Dropped'];
	    
	    /*if(gProfileName =='Rep')
	    {
	    	rp = [SELECT Name FROM Report where Name='Call Plan View Report Added/Dropped'];
	    }
	    else */
	    if(gProfileName =='DM')
	    {
	    	rp = [SELECT Name FROM Report where Name='Call Plan View Report Added/Dropped DM'];
	    }
	    else if(gProfileName =='RM')
	    {
	    	rp = [SELECT Name FROM Report where Name='Call Plan View Report Added/Dropped RM'];
	    }
	    else if(gProfileName =='HO')
	    {
	    	rp = [SELECT Name FROM Report where Name='Call Plan View Report Added/Dropped HO'];
	    }
	    else
	    {
	    	rp = [SELECT Name FROM Report where Name='Call Plan View Report Added/Dropped'];
	    }
	    
	    if(rp.size()>0) 
	    {
	      PageReference pg = new PageReference ('/'+rp[0].Id+'?pv0='+loggedInPositionclientcode);
	      
	      pg.setRedirect(true);
	      
	      return pg;      
	    }
    }
    else if(reportname=='Call Plan Summary Calls Changed'){
	    List<Report> rp;// = [SELECT Name FROM Report where Name='Call Plan View Report Calls Changed'];
	    
	    /*if(gProfileName =='Rep')
	    {
	    	rp = [SELECT Name FROM Report where Name='Call Plan View Report Calls Changed'];
	    }
	    else */
	    if(gProfileName =='DM')
	    {
	    	rp = [SELECT Name FROM Report where Name='Call Plan View Report Calls Changed DM'];
	    }
	    else if(gProfileName =='RM')
	    {
	    	rp = [SELECT Name FROM Report where Name='Call Plan View Report Calls Changed RM'];
	    }
	    else if(gProfileName =='HO')
	    {
	    	rp = [SELECT Name FROM Report where Name='Call Plan View Report Calls Changed HO'];
	    }
	    else
	    {
	    	rp = [SELECT Name FROM Report where Name='Call Plan View Report Calls Changed'];
	    }
	    
	    if(rp.size()>0) 
	    {
	      PageReference pg = new PageReference ('/'+rp[0].Id+'?pv0='+loggedInPositionclientcode);
	      
	      pg.setRedirect(true);
	      
	      return pg;      
	    }
    }
    
    else if(reportname=='Original Call Plan'){
	    List<Report> rp;// = [SELECT Name FROM Report where Name='Call Plan View Report'];
	    
	    /*if(gProfileName =='Rep')
	    {
	    	rp = [SELECT Name FROM Report where Name='Call Plan View Report'];
	    }
	    else */
	    if(gProfileName =='DM')
	    {
	    	rp = [SELECT Name FROM Report where Name='Original Call Plan View Report DM'];
	    }
	    else if(gProfileName =='RM')
	    {
	    	rp = [SELECT Name FROM Report where Name='Original Call Plan View Report RM'];
	    }
	    else if(gProfileName =='HO')
	    {
	    	rp = [SELECT Name FROM Report where Name='Original Call Plan View Report HO'];
	    }
	    else
	    {
	    	rp = [SELECT Name FROM Report where Name='Original Call Plan View Report'];
	    }
	    
	    if(rp.size()>0) 
	    {
	      PageReference pg = new PageReference ('/'+rp[0].Id+'?pv0='+loggedInPositionclientcode);
	      
	      pg.setRedirect(true);
	      
	      return pg;      
	    }
    }
    
    
    
    return null;
  }
}
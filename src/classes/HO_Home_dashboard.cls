public class HO_Home_dashboard {
	// with sharing
	 public string territoryName {get;set;}
    public string teamInsName {get;set;}
    public string posType{get;set;}
    public String gProfileName{get;set;}
    
    public Integer totalOrignalTarget{get;set;}
    public Integer TotalproposedTarget{get;set;}
    public Integer totalproposedCall{get;set;}
	public Integer totalOrignalCall{get;set;}
   
    public map<string,Integer>mapToAddCount{get;set;}
    public string rootPositionId='';
    public list<wrapperclass> AddList{get;set;}
    public list<wrapperclass> AddListWithTerritory{get;set;}
    public list<wrapperclass> AddListWithReason{get;set;}
    public list<wrapperclass> DropListWithReason{get;set;}
    public list<wrapperclass> DropListWithTerritory{get;set;}
    public map<string,Integer>mapToDropCount{get;set;}
    public list<wrapperclass> DropList{get;set;}
    public static Integer totaladd {get;set;}
    public static Integer totaldrop {get;set;}
	public List<AxtriaSalesIQTM__User_Access_Permission__c> loggedInUserData  {get;set;}
	public list<SelectOption > regionList                    {get; set;}
    public string regionSelected                             {get; set;}
    
    public list<SelectOption> districtList                   {get; set;}
    public string districtSelected                           {get; set;}
    public map<ID, list<AxtriaSalesIQTM__Position__c>> mapParentIdPositionId  {get; set;}
    public List<wrapperclass1> tablelist {get;set;}
    public  map<string,Integer>statusmap = new   map<string,Integer>();    
    public map<String,AxtriaSalesIQTM__CIM_Position_Metric_Summary__c> mapTerritoryNameToTotalTarget=new  map<string,AxtriaSalesIQTM__CIM_Position_Metric_Summary__c>();
    
	public  HO_Home_dashboard(){
	
		totaladd=0;
		totaldrop=0;
		loggedInUserData     = new List<AxtriaSalesIQTM__User_Access_Permission__c>();
        loggedInUserData = SalesIQUtility.getUserAccessPermistion(Userinfo.getUserId());
        gProfileName = [Select Id,Name from Profile where Id=:UserInfo.getProfileId()].Name;
        
        system.debug('loggedInUserData++++'+loggedInUserData);
        
        
        
        if(gProfileName =='HO')
            {
                  
                   mapParentIdPositionId = new map<ID, list<AxtriaSalesIQTM__Position__c>>();
                List<AxtriaSalesIQTM__Position__c> allRecs = [select id, name, AxtriaSalesIQTM__Parent_Position__c, AxtriaSalesIQTM__Parent_Position__r.id, AxtriaSalesIQTM__Position_Type__c from AxtriaSalesIQTM__Position__c order by name];
                system.debug('****allRecs '+allRecs);
                List<AxtriaSalesIQTM__Position__c> regionRecs = [select id, name, AxtriaSalesIQTM__Parent_Position__c, AxtriaSalesIQTM__Parent_Position__r.id, AxtriaSalesIQTM__Position_Type__c from AxtriaSalesIQTM__Position__c where AxtriaSalesIQTM__Parent_Position__c =: loggedInUserData[0].AxtriaSalesIQTM__Position__c order by name];
                system.debug('****regionRecs '+regionRecs);
                
                for(AxtriaSalesIQTM__Position__c pos : allRecs)
                {
                    if(mapParentIdPositionId.get(pos.AxtriaSalesIQTM__Parent_Position__r.id) != null){
                        list<AxtriaSalesIQTM__Position__c> positions = new list<AxtriaSalesIQTM__Position__c>(mapParentIdPositionId.get(pos.AxtriaSalesIQTM__Parent_Position__r.id));
                        positions.add(pos);
                        mapParentIdPositionId.put(pos.AxtriaSalesIQTM__Parent_Position__r.id, positions);
                    }
                    else{
                        list<AxtriaSalesIQTM__Position__c> positions = new list<AxtriaSalesIQTM__Position__c>();
                        positions.add(pos);
                        mapParentIdPositionId.put(pos.AxtriaSalesIQTM__Parent_Position__r.id, positions);
                    }
                }
                
                
                
                 system.debug('****mapParentIdPositionId '+mapParentIdPositionId);
                regionList = new list<SelectOption >();
                
                if(regionRecs != null && regionRecs.size() > 0 )
                    regionSelected = regionRecs[0].id;
                    
                for(AxtriaSalesIQTM__Position__c pos : regionRecs){                  
                    regionList.add(new SelectOption(pos.id,pos.name));
                }
                system.debug('****regionList '+regionList);
                
                districtListChange();
               
				getTerritorybyLoggedUser();
	
	
	}
	
	
	}
	
	
	public void districtListChange()
    {
        system.debug('********distrcit list change function called');
        
        boolean setTerritory = false;
        districtList = new list<SelectOption>();
        
        if(mapParentIdPositionId.get(regionSelected) != null){
            for(AxtriaSalesIQTM__Position__c pos : mapParentIdPositionId.get(regionSelected)){
                districtList.add(new SelectOption(pos.id,pos.name));
                if(setTerritory == false)
                {
                    districtSelected = pos.id;
                    setTerritory = true;
                }
            }
        }
        
        system.debug('****districtList '+districtList);
       // getTerritorybyLoggedUser();
        //territoryListChange();
        
    }
	 
	 public void getTerritorybyLoggedUser(){
        
      
//       droppedlist = new List<wrapperclassadded>();
        
        
        
        rootPositionId='';
        AxtriaSalesIQTM__User_Access_Permission__c access = [Select Id, AxtriaSalesIQTM__Position__c,
                                                AxtriaSalesIQTM__Position__r.Name,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__X_Max__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__X_Min__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Y_Max__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Y_Min__c, AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Position_Type__c,   
                                                AxtriaSalesIQTM__Team_Instance__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__c, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Team__r.Name, AxtriaSalesIQTM__Team_Instance__r.Name, AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Type__c ,AxtriaSalesIQTM__Map_Access_Position__c
                                                From AxtriaSalesIQTM__User_Access_Permission__c 
                                                where AxtriaSalesIQTM__Is_Active__c = true AND AxtriaSalesIQTM__User__c =: UserInfo.getUserId() limit 1]; 
            
        system.debug('#### Access Permission : '+ access.Id);
        
         
        if(gProfileName=='HO' || gProfileName=='RM'){
        
            rootPositionId=districtSelected;
        
        }
        else{
            rootPositionId = access.AxtriaSalesIQTM__Position__c;
        }
        
        territoryName = access.AxtriaSalesIQTM__Position__r.Name;
        teamInsName = access.AxtriaSalesIQTM__Team_Instance__c;
        system.debug('#### poition and team instance : '+ rootPositionId +'     '+teamInsName+'   '+gProfileName);
        PosType = access.AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Position_Type__c;
            
        list<AxtriaSalesIQTM__Position_Team_Instance__c> allAccessiblePositions = [Select id,AxtriaSalesIQTM__Position_ID__c,AxtriaSalesIQTM__Team_Instance_ID__c From AxtriaSalesIQTM__Position_Team_Instance__c p 
                                                        where (AxtriaSalesIQTM__Position_ID__c =: rootPositionId 
                                                        OR AxtriaSalesIQTM__Position_ID__r.AxtriaSalesIQTM__Parent_Position__c =: rootPositionId 
                                                        OR AxtriaSalesIQTM__Position_ID__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__c =: rootPositionId 
                                                        OR AxtriaSalesIQTM__Position_ID__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__r.AxtriaSalesIQTM__Parent_Position__c =: rootPositionId 
                                                        
                                                        ) 
                                                        and AxtriaSalesIQTM__Position_ID__r.AxtriaSalesIQTM__inactive__c = false 
                                                        and AxtriaSalesIQTM__Team_Instance_ID__c =: teamInsName
                                                        and  AxtriaSalesIQTM__Position_ID__r.AxtriaSalesIQTM__Position_type__c='Territory'
                                                        order by Name];
        
        set<string> positionTeamInstanceIds = new set<string>();
        for(AxtriaSalesIQTM__Position_Team_Instance__c teamPos : allAccessiblePositions){
            positionTeamInstanceIds.add(teamPos.id);
        }
        
        set<string> positions = new set<string>();
        for(AxtriaSalesIQTM__Position_Team_Instance__c teamPos : allAccessiblePositions){
            positions.add(teamPos.AxtriaSalesIQTM__Position_ID__c);
        }
        set<string> TeamInstances = new set<string>();
        for(AxtriaSalesIQTM__Position_Team_Instance__c teamPos : allAccessiblePositions){
            TeamInstances.add(teamPos.AxtriaSalesIQTM__Team_Instance_ID__c);
        }
        
        system.debug('--positionTeamInstanceIds '+positionTeamInstanceIds.size()+'     positionTeamInstanceIds   '+positionTeamInstanceIds);
        getTotalTargetAndCall(positionTeamInstanceIds,positions,TeamInstances);
       
    }  
    public void getTotalTargetAndCall(set<string> Territory,set<string> positions,set<string> TeamInstances)
      {
      	    statusmap.put('Submitted',0);
        	statusmap.put('In Progress',0);
        	statusmap.put('Not Started',0);
        	totaladd=0;
			totaldrop=0;
			totalOrignalTarget=0;
			TotalproposedTarget=0;
			totalproposedCall=0;
			totalOrignalCall=0;
      	   AddListWithTerritory=new list<wrapperclass>();	
      	   DropListWithTerritory=new list<wrapperclass>();	
      	   AddListWithReason=new list<wrapperclass>();
      	   DropListWithReason=new list<wrapperclass>();	
      	   AddList=new list<wrapperclass>();	 
      	   mapToAddCount=new  map<string,Integer>();
      	   mapToAddCount.put('1-25',0);
      	   mapToAddCount.put('25-50',0);
      	   mapToAddCount.put('50-75',0);
      	   mapToAddCount.put('75-100',0);
      	   mapToAddCount.put('100-125',0);
      	   mapToAddCount.put('>125',0);
      	   DropList=new list<wrapperclass>();	 
      	   mapToDropCount=new  map<string,Integer>();
      	   mapToDropCount.put('0',0);
      	   mapToDropCount.put('1-20',0);
      	   mapToDropCount.put('20-40',0);
      	   mapToDropCount.put('40-60',0);
      	   mapToDropCount.put('60-80',0);
      	   mapToDropCount.put('80-100',0);
      	   mapToDropCount.put('>=100',0);
      	   integer i=0;
      	for(AggregateResult pacp:[select count(id) Total,AxtriaSalesIQTM__Position_Team_Instance__r.AxtriaSalesIQTM__Position_ID__r.Name posName, AxtriaSalesIQTM__Position_Team_Instance__r.AxtriaSalesIQTM__Position_ID__r.AxtriaSalesIQTM__Client_Position_Code__c code,AxtriaSalesIQTM__Position_Team_Instance__c posTeamInst from AxtriaSalesIQTM__Position_Account_Call_Plan__c  where AxtriaSalesIQTM__Position_Team_Instance__c in:Territory and AxtriaSalesIQTM__isAccountTarget__c =false and AxtriaSalesIQTM__isincludedCallPlan__c =true group by AxtriaSalesIQTM__Position_Team_Instance__c,AxtriaSalesIQTM__Position_Team_Instance__r.AxtriaSalesIQTM__Position_ID__r.AxtriaSalesIQTM__Client_Position_Code__c ,AxtriaSalesIQTM__Position_Team_Instance__r.AxtriaSalesIQTM__Position_ID__r.Name order by count(id) desc limit 10])
       {
            String posTeamInstance=String.valueOf(pacp.get('posTeamInst'));
            Integer value=Integer.valueOf(pacp.get('Total'));
            //dropList.put(posTeamInstance,value);
           	string territoryName= String.valueOf(pacp.get('posName'));
           	string territoryCode= String.valueOf(pacp.get('code'));
           	wrapperclass obj1=new wrapperclass(value,'',territoryCode,territoryName);
	      	AddListWithTerritory.add(obj1);
            if(value>=1 && value<25){
            
            	Integer temp=mapToAddCount.get('1-25');
            	temp=temp+1;
            	mapToAddCount.put('1-25',temp);
            
            }
            if(value>=25 && value<50){
            
            	Integer temp=mapToAddCount.get('25-50');
            	temp=temp+1;
            	mapToAddCount.put('25-50',temp);
            
            }
            if(value>=50 && value<75){
            
            	Integer temp=mapToAddCount.get('50-75');
            	temp=temp+1;
            	mapToAddCount.put('50-75',temp);
            
            }
            if(value>=75 && value<100){
            
            	Integer temp=mapToAddCount.get('75-100');
            	temp=temp+1;
            	mapToAddCount.put('75-100',temp);
            
            }
            if(value>=100 && value<125){
            
            	Integer temp=mapToAddCount.get('100-125');
            	temp=temp+1;
            	mapToAddCount.put('100-125',temp);
            
            }
             if(value>=125 ){
            
            	Integer temp=mapToAddCount.get('>=125');
            	temp=temp+1;
            	mapToAddCount.put('>=125',temp);
       
       }
      
       }
	     
      for(AggregateResult pacp:[select count(id) Total,AxtriaSalesIQTM__Position_Team_Instance__r.AxtriaSalesIQTM__Position_ID__r.Name posName, AxtriaSalesIQTM__Position_Team_Instance__r.AxtriaSalesIQTM__Position_ID__r.AxtriaSalesIQTM__Client_Position_Code__c code,AxtriaSalesIQTM__Position_Team_Instance__c posTeamInst from AxtriaSalesIQTM__Position_Account_Call_Plan__c  where AxtriaSalesIQTM__Position_Team_Instance__c in:Territory and AxtriaSalesIQTM__isAccountTarget__c =true and AxtriaSalesIQTM__isincludedCallPlan__c =false group by AxtriaSalesIQTM__Position_Team_Instance__c,AxtriaSalesIQTM__Position_Team_Instance__r.AxtriaSalesIQTM__Position_ID__r.Name , AxtriaSalesIQTM__Position_Team_Instance__r.AxtriaSalesIQTM__Position_ID__r.AxtriaSalesIQTM__Client_Position_Code__c  order by count(id) desc limit 10])
       {
            String posTeamInstance=String.valueOf(pacp.get('posTeamInst'));
            Integer value=Integer.valueOf(pacp.get('Total'));
            //dropList.put(posTeamInstance,value);
            string territoryName= String.valueOf(pacp.get('posName'));
           	string territoryCode= String.valueOf(pacp.get('code'));
           	wrapperclass obj1=new wrapperclass(value,'',territoryCode,territoryName);
	      	DropListWithTerritory.add(obj1);
             i++;
            if(value>=1 && value<20){
            
            	Integer temp=mapToDropCount.get('1-20');
            	temp=temp+1;
            	mapToDropCount.put('1-20',temp);
            
            }
            if(value>=20 && value<40){
            
            	Integer temp=mapToDropCount.get('20-40');
            	temp=temp+1;
            	mapToDropCount.put('20-40',temp);
            
            }
            if(value>=40 && value<60){
            
            	Integer temp=mapToDropCount.get('40-60');
            	temp=temp+1;
            	mapToDropCount.put('40-60',temp);
            
            }
            if(value>=60 && value<80){
            
            	Integer temp=mapToDropCount.get('60-80');
            	temp=temp+1;
            	mapToDropCount.put('60-80',temp);
            
            }
            if(value>=80 && value<100){
            
            	Integer temp=mapToDropCount.get('80-100');
            	temp=temp+1;
            	mapToDropCount.put('80-100',temp);
            
            }
            if(value>=100){
            
            	Integer temp=mapToDropCount.get('>=100');
            	temp=temp+1;
            	mapToDropCount.put('>=100',temp);
            
            }
            
      
       }
	      for(string key:mapToAddCount.keySet()){
	      
	      	wrapperclass obj=new wrapperclass(mapToAddCount.get(key),key,'','');
	      		AddList.add(obj);
	      }
	      
      	 for(string key:mapToDropCount.keySet()){
	      
	      	integer u=Territory.size();
	      	if(key=='0')
	      	{
	      		Integer k=u-i;
	      		wrapperclass obj=new wrapperclass(k,key,'','');
	      		DropList.add(obj);
	      	}
	      	else{
	      		
	      		wrapperclass obj=new wrapperclass(mapToDropCount.get(key),key,'','');
	      		DropList.add(obj);
	      	}
	      }
      	
      	 for(AggregateResult pacp:[select count(id) Total,AxtriaSalesIQTM__Picklist3_Updated__c reasonCode from AxtriaSalesIQTM__Position_Account_Call_Plan__c  where AxtriaSalesIQTM__Position_Team_Instance__c in:Territory and AxtriaSalesIQTM__isAccountTarget__c =false and AxtriaSalesIQTM__isincludedCallPlan__c =true group by AxtriaSalesIQTM__Picklist3_Updated__c  order by count(id) desc limit 10])
       	 {
      		 Integer value=Integer.valueOf(pacp.get('Total'));
      		 totaladd=totaladd+value;
            //dropList.put(posTeamInstance,value);
            string reason= String.valueOf(pacp.get('reasonCode'));
            wrapperclass obj=new wrapperclass(value,reason,'','');
           	AddListWithReason.add(obj);
       	 }
       	 
       	  for(AggregateResult pacp:[select count(id) Total,AxtriaSalesIQTM__Picklist3_Updated__c reasonCode from AxtriaSalesIQTM__Position_Account_Call_Plan__c  where AxtriaSalesIQTM__Position_Team_Instance__c in:Territory and AxtriaSalesIQTM__isAccountTarget__c =true and AxtriaSalesIQTM__isincludedCallPlan__c =false group by AxtriaSalesIQTM__Picklist3_Updated__c order by count(id) desc limit 10 ])
       	 {
      		 Integer value=Integer.valueOf(pacp.get('Total'));
            //dropList.put(posTeamInstance,value);
             totaldrop=totaldrop+value;
            string reason= String.valueOf(pacp.get('reasonCode'));
            wrapperclass obj=new wrapperclass(value,reason,'','');
           	DropListWithReason.add(obj);
       	 }
       	  	wrapperclass obj=new wrapperclass(totaladd,'Total Add','','');
           	AddListWithReason.add(obj);
           	wrapperclass obj1=new wrapperclass(totaldrop,'Total Drop','','');
           	DropListWithReason.add(obj1);
      
      //---------------------------------

        /*for(AggregateResult allaggpushedCheck : [select count(id) countRecs, AxtriaSalesIQTM__Position_Team_Instance__c posTeamInst  from AxtriaSalesIQTM__Position_Account_Call_Plan__c where original_PTI__c  in: Territory  and ((lastApproved_PTI__c not in: Territory  and AxtriaSalesIQTM__Position_Team_Instance__c  in: Territory ) or (AxtriaSalesIQTM__Position_Team_Instance__c not in: Territory ))   group by AxtriaSalesIQTM__Position_Team_Instance__c  ])
        {
            String posteamInstance=String.valueOf(allaggpushedCheck.get('posTeamInst'));
            Integer value=Integer.valueOf(allaggpushedCheck.get('countRecs'));
            pushList.put(posteamInstance,value);
        }*/
       
        //****************** Total Calls*********************************************\\
        
        List<AxtriaSalesIQTM__CIM_Position_Metric_Summary__c> memb1 = new List<AxtriaSalesIQTM__CIM_Position_Metric_Summary__c>();  
        list<AxtriaSalesIQTM__CIM_Config__c> cim1 = [select id , Name from AxtriaSalesIQTM__CIM_Config__c where Name ='Total Calls' and AxtriaSalesIQTM__team_instance__c=:teamInsName];
        String sql1 = 'select AxtriaSalesIQTM__Proposed__c,last_cycle__c,AxtriaSalesIQTM__Position_Team_Instance__c, AxtriaSalesIQTM__Original__c,AxtriaSalesIQTM__Position_Team_Instance__r.AxtriaSalesIQTM__Position_ID__c,AxtriaSalesIQTM__Position_Team_Instance__r.AxtriaSalesIQTM__Position_ID__r.AxtriaSalesIQTM__Client_Position_Code__c,AxtriaSalesIQTM__Position_Team_Instance__r.AxtriaSalesIQTM__Position_ID__r.Name,AxtriaSalesIQTM__Position_Team_Instance__r.AxtriaSalesIQTM__isCallPlanSubmitted__c,AxtriaSalesIQTM__Position_Team_Instance__r.Call_Plan_Status__c, AxtriaSalesIQTM__Position_Team_Instance__r.isCallPlanSaved__c, AxtriaSalesIQTM__CIM_Config__r.Name from AxtriaSalesIQTM__CIM_Position_Metric_Summary__c where AxtriaSalesIQTM__CIM_Config__c =: cim1 and AxtriaSalesIQTM__Position_Team_Instance__c in: Territory order by AxtriaSalesIQTM__Position_Team_Instance__r.AxtriaSalesIQTM__Position_ID__r.Name';
        memb1 = Database.Query(sql1);   
        
        //---------------------------------
        
        
        
         map<string,AxtriaSalesIQTM__Change_Request__c >CRMap=new  map<string,AxtriaSalesIQTM__Change_Request__c >();

        String CRSql = 'select id,name,AxtriaSalesIQTM__Destination_Position__c from AxtriaSalesIQTM__Change_Request__c where AxtriaSalesIQTM__Destination_Position__c in:positions and AxtriaSalesIQTM__Team_Instance_ID__c in:teaminstances Order By AxtriaSalesIQTM__Destination_Position__c,CreatedDate desc';
        List<AxtriaSalesIQTM__Change_Request__c> AllCR = Database.Query(CRSql); 
        if (AllCR.size()>0){
            for (AxtriaSalesIQTM__Change_Request__c tempCR : AllCR){
                if(!CRMap.containskey(tempCR.AxtriaSalesIQTM__Destination_Position__c)){
                    CRMap.put(tempCR.AxtriaSalesIQTM__Destination_Position__c,tempCR);
                }
            }
        }
        system.debug('++++++++++++CRMap'+CRMap);
         map<string,string >userposMap=new  map<string,string >();
         
         
        String upossql = 'Select AxtriaSalesIQTM__User__c,AxtriaSalesIQTM__Position__c from AxtriaSalesIQTM__User_Access_Permission__c where AxtriaSalesIQTM__Position__c in :positions';
        List <AxtriaSalesIQTM__User_Access_Permission__c> userpos = Database.Query(upossql);
        
        for(AxtriaSalesIQTM__User_Access_Permission__c tempuserpos : userpos){
            String uid=tempuserpos.AxtriaSalesIQTM__User__c;
            
            String loginSql = 'select count(id) countoflogin from LoginHistory where userid = \''+uid+'\' group by userid';
            List<AggregateResult> templogin = Database.Query(loginSql); 
            
            if (templogin.size()>0){
                userposMap.put(tempuserpos.AxtriaSalesIQTM__Position__c,String.valueOf(templogin[0].get('countoflogin')));
            }
            system.debug('uid----->>'+uid);
            system.debug('templogin----->>'+templogin);
            
        }
        
         
         
          //****************** Total Target*********************************************\\
         
          system.debug('Territory+++++++++++++++++++'+Territory);
         List<AxtriaSalesIQTM__CIM_Position_Metric_Summary__c> memb = new List<AxtriaSalesIQTM__CIM_Position_Metric_Summary__c>();  
         list<AxtriaSalesIQTM__CIM_Config__c> cim = [select id , Name from AxtriaSalesIQTM__CIM_Config__c where Name ='No of Targets' and AxtriaSalesIQTM__team_instance__c=:teamInsName];
         String sql = 'select AxtriaSalesIQTM__Proposed__c,AxtriaSalesIQTM__Position_Team_Instance__c, last_cycle__c,AxtriaSalesIQTM__Original__c,AxtriaSalesIQTM__Position_Team_Instance__r.AxtriaSalesIQTM__Position_ID__r.Name,AxtriaSalesIQTM__Position_Team_Instance__r.AxtriaSalesIQTM__isCallPlanSubmitted__c, AxtriaSalesIQTM__CIM_Config__r.Name from AxtriaSalesIQTM__CIM_Position_Metric_Summary__c where AxtriaSalesIQTM__CIM_Config__c =: cim and AxtriaSalesIQTM__Position_Team_Instance__c in: Territory order by AxtriaSalesIQTM__Position_Team_Instance__r.AxtriaSalesIQTM__Position_ID__r.Name';
         memb = Database.Query(sql);
            
         for(AxtriaSalesIQTM__CIM_Position_Metric_Summary__c temp : memb)
         { 
                mapTerritoryNameToTotalTarget.put(temp.AxtriaSalesIQTM__Position_Team_Instance__c,temp);
         
         }
         //********************Add and Drop  Target********************************\\
        map<String,Integer >addMapList=new   map<string,Integer>();
        map<string,Integer >dropMapList=new  map<string,Integer >();
        
        for(AggregateResult pacp:[select count(id) Total, AxtriaSalesIQTM__Position_Team_Instance__c posTeamInst  from AxtriaSalesIQTM__Position_Account_Call_Plan__c  where AxtriaSalesIQTM__Position_Team_Instance__c in:Territory and AxtriaSalesIQTM__isAccountTarget__c =false and AxtriaSalesIQTM__isincludedCallPlan__c =true group by AxtriaSalesIQTM__Position_Team_Instance__c  ])
        {
            system.debug('AggregateResult'+pacp);
            String posteamInstance=String.valueOf(pacp.get('posTeamInst'));
            Integer value=Integer.valueOf(pacp.get('Total'));
            addMapList.put(posteamInstance,value);
        }
        
      for(AggregateResult pacp:[select count(id) Total,AxtriaSalesIQTM__Position_Team_Instance__c posTeamInst from AxtriaSalesIQTM__Position_Account_Call_Plan__c  where AxtriaSalesIQTM__Position_Team_Instance__c in:Territory and AxtriaSalesIQTM__isAccountTarget__c =true and AxtriaSalesIQTM__isincludedCallPlan__c =false group by AxtriaSalesIQTM__Position_Team_Instance__c ])
       {
            String posTeamInstance=String.valueOf(pacp.get('posTeamInst'));
            Integer value=Integer.valueOf(pacp.get('Total'));
            dropMapList.put(posTeamInstance,value);
            
       
       }
        
        //---------------------------
         tablelist= new list<wrapperclass1>();
         
         map<String,Integer> originalCallsMap = new map<String,Integer>();
        map<String,Integer> updatedCallsMap = new map<String,Integer>();
        map<String,Integer> updatedTargetMap = new map<String,Integer>();
        map<String,Integer> originalTargetMap = new map<String,Integer>();
        for (AggregateResult a: [select sum(Original_Calls_Numeric__c) calls,AxtriaSalesIQTM__Position_Team_Instance__c posteam from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__isAccountTarget__c = true group by AxtriaSalesIQTM__Position_Team_Instance__c])
        {
            originalCallsMap.put(String.valueOf(a.get('posteam')),Integer.valueOf(a.get('calls')));
        }
        for (AggregateResult a: [select sum(Calls_Numeric__c) calls,AxtriaSalesIQTM__Position_Team_Instance__c posteam  from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__isIncludedCallPlan__c = true  group by AxtriaSalesIQTM__Position_Team_Instance__c])
        {
            updatedCallsMap.put(String.valueOf(a.get('posteam')),Integer.valueOf(a.get('calls')));
        }
        for (AggregateResult a: [select count(id) targets,AxtriaSalesIQTM__Position_Team_Instance__c posteam  from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__isIncludedCallPlan__c = true  group by AxtriaSalesIQTM__Position_Team_Instance__c])
        {
            updatedTargetMap.put(String.valueOf(a.get('posteam')),Integer.valueOf(a.get('targets')));
        }
        for (AggregateResult a: [select count(id) targets,AxtriaSalesIQTM__Position_Team_Instance__c posteam  from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__isAccountTarget__c = true  group by AxtriaSalesIQTM__Position_Team_Instance__c])
        {
            originalTargetMap.put(String.valueOf(a.get('posteam')),Integer.valueOf(a.get('targets')));
        }  
       
        for(AxtriaSalesIQTM__CIM_Position_Metric_Summary__c temp : memb1)
        { 
                //mapTerritoryNameToTotalCalls.put(temp.AxtriaSalesIQTM__Position_Team_Instance__c,temp);
                string TerritoryName=temp.AxtriaSalesIQTM__Position_Team_Instance__r.AxtriaSalesIQTM__Position_ID__r.Name;
                string Territorycode=temp.AxtriaSalesIQTM__Position_Team_Instance__r.AxtriaSalesIQTM__Position_ID__r.AxtriaSalesIQTM__Client_Position_Code__c;
                string posid=temp.AxtriaSalesIQTM__Position_Team_Instance__r.AxtriaSalesIQTM__Position_ID__c;
                
                string CRname ='N/A';
                string CRid ='N/A';
                
                if(CRMap.containskey(posid)){
                    CRname=CRMap.get(posid).name;
                    CRid=CRMap.get(posid).id;
                }
                
                
                String logincount='0';//uid+'------'+
                if(userposMap.containskey(posid)){
                    logincount=userposMap.get(posid);
                }
                    
                    //string logincount =templogin[0].countoflogin;
                    
                    /*if (templogin.size()>0){
                        logincount=String.valueOf(templogin[0].get('countoflogin'));//uid+'  '+
                    }*/
                
                
                string FeedbackStatus='';//Not Submitted
                
                
                FeedbackStatus = temp.AxtriaSalesIQTM__Position_Team_Instance__r.Call_Plan_Status__c;
                
               /*
                
                if(temp.AxtriaSalesIQTM__Position_Team_Instance__r.AxtriaSalesIQTM__isCallPlanSubmitted__c)
                {
                    FeedbackStatus='Submitted';
                    
                    if(statusmap.get('Submitted') != 0){
                            integer rangecount = Integer.Valueof(statusmap.get('Submitted'));
                            statusmap.put('Submitted', rangecount+1);
                    }
                   else
                    {
                            statusmap.put('Submitted', 1);
                    }
                    
                    
                }
                else{
                    if(temp.AxtriaSalesIQTM__Position_Team_Instance__r.isCallPlanSaved__c)
                    {
                        FeedbackStatus='In Progress';
                        if(statusmap.get('In Progress') != 0){
                                integer rangecount = Integer.Valueof(statusmap.get('In Progress'));
                                statusmap.put('In Progress', rangecount+1);
                        }
                       else
                        {
                                statusmap.put('In Progress', 1);
                        }
                        
                    }
                    else{
                        FeedbackStatus='Not Started';
                        if(statusmap.get('Not Started') != 0){
                                integer rangecount = Integer.Valueof(statusmap.get('Not Started'));
                                statusmap.put('Not Started', rangecount+1);
                        }
                       else
                        {
                                statusmap.put('Not Started', 1);
                        }
                        
                    }
                }
                */
                
                Integer add=addMapList.get(temp.AxtriaSalesIQTM__Position_Team_Instance__c);
                Integer drop=dropMapList.get(temp.AxtriaSalesIQTM__Position_Team_Instance__c);
                if(add==null){
                
                    add=0;
                
                }
                if(drop==null){
                
                    drop=0;
                }
                
                
        
        AggregateResult aggAdded = [select count(id) countRecs from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__isAccountTarget__c = false and AxtriaSalesIQTM__isIncludedCallPlan__c = true and AxtriaSalesIQTM__Position__c =:posid];
                Integer QueryphyAdded = Integer.valueof(aggAdded.get('countRecs'));              
            
                AggregateResult aggDropped = [select count(id) countRecs from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__isAccountTarget__c = true and AxtriaSalesIQTM__isIncludedCallPlan__c = false and AxtriaSalesIQTM__Position__c =:posid];
                Integer QueryphyDropped = Integer.valueof(aggDropped.get('countRecs'));
                
        
        AggregateResult aggResultUpdated  = [select count(id) targets, sum(Calls_Numeric__c) calls from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__isIncludedCallPlan__c = true and AxtriaSalesIQTM__Position__c =:posid];
           Integer QueryUpdatedTargets = Integer.valueOf(aggResultUpdated.get('targets'));
           Integer QueryUpdatedCalls = Integer.valueOf(aggResultUpdated.get('calls'));
                
                String QueryupdatedCallsstring;
            if(String.valueof(QueryUpdatedCalls)!=null){
                QueryupdatedCallsstring = String.valueof(QueryUpdatedCalls);
            }else{
                QueryupdatedCallsstring = '0';
            }
            
            String QueryUpdatedTargetsstring;
            if(String.valueof(QueryUpdatedTargets)!=null){
                QueryUpdatedTargetsstring = String.valueof(QueryUpdatedTargets);
            }else{
                QueryUpdatedTargetsstring = '0';
            }
                
                AxtriaSalesIQTM__CIM_Position_Metric_Summary__c CIMMS=mapTerritoryNameToTotalTarget.get(temp.AxtriaSalesIQTM__Position_Team_Instance__c);
                
                decimal Disruption=QueryphyAdded+QueryphyDropped;//add+drop;
                
                String posTeam = temp.AxtriaSalesIQTM__Position_Team_Instance__r.id;
            system.debug('posTeamIns : ' + posTeam);
            String originalCalls;
            if(String.valueof(originalCallsMap.get(posTeam))!=null){
                originalCalls = String.valueof(originalCallsMap.get(posTeam));
            }else{
                originalCalls = '0';
            }
            String updatedCalls;
            if(String.valueof(updatedCallsMap.get(posTeam))!=null){
                updatedCalls = String.valueof(updatedCallsMap.get(posTeam));
            }else{
                updatedCalls = '0';
            }
            String updatedTargets;
            if(String.valueof(updatedTargetMap.get(posTeam))!=null){
                updatedTargets = String.valueof(updatedTargetMap.get(posTeam));
            }else{
                updatedTargets = '0';
            }
            String originalTargets;
            if(String.valueof(originalTargetMap.get(posTeam))!=null){
                originalTargets = String.valueof(originalTargetMap.get(posTeam));
            }else{
                originalTargets = '0';
            }
                
                Integer lastcycleTarget=Integer.valueOf(CIMMS.last_cycle__c);
                if(lastcycleTarget!=0){Disruption=(Disruption/lastcycleTarget)*100;}
                Disruption=Disruption.setScale(2);
                decimal changeValue =0;
                /* if(decimal.valueOf(temp.AxtriaSalesIQTM__Original__c)!=0){
                changeValue=(decimal.valueOf(temp.AxtriaSalesIQTM__Proposed__c)-decimal.valueOf(temp.AxtriaSalesIQTM__Original__c))/(decimal.valueOf(temp.AxtriaSalesIQTM__Original__c));
                }*/
                if(decimal.valueOf(originalCalls)!=0){
                changeValue=(decimal.valueOf(QueryupdatedCallsstring)-decimal.valueOf(originalCalls))/(decimal.valueOf(originalCalls));
                }
               changeValue=(changeValue*100).setScale(2);
                wrapperclass1 wrapObj=new  wrapperclass1(CRname,CRid,posid,TerritoryName ,Territorycode,FeedbackStatus,logincount,temp.last_cycle__c ,/*String.valueOf(updatedCalls)*/QueryUpdatedCallsstring ,
            String.valueOf(originalCalls),CIMMS.last_cycle__c,/*String.valueof(updatedTargets)*/QueryUpdatedTargetsstring , String.valueof(originalTargets),
            /*,temp.last_cycle__c ,temp.AxtriaSalesIQTM__Proposed__c ,
                temp.AxtriaSalesIQTM__Original__c,CIMMS.last_cycle__c,CIMMS.AxtriaSalesIQTM__Proposed__c , CIMMS.AxtriaSalesIQTM__Original__c,*/ /*add , drop,*/QueryphyAdded,QueryphyDropped , Disruption,changeValue);
                
                totalOrignalTarget=totalOrignalTarget+Integer.valueOf(CIMMS.AxtriaSalesIQTM__Original__c);
                TotalproposedTarget=TotalproposedTarget+Integer.valueOf(CIMMS.AxtriaSalesIQTM__Proposed__c);
                totalproposedCall=totalproposedCall+Integer.valueOf(temp.AxtriaSalesIQTM__Proposed__c);
                totalOrignalCall=totalOrignalCall+Integer.valueOf(temp.AxtriaSalesIQTM__Original__c);
                tablelist.add(wrapObj);
                
                
        }
      
      
      }
    
     public class wrapperclass1{
        
        public string changerequest                 {get;set;}
        public string changerequestid               {get;set;}
        public string positionid                    {get;set;}
        public string Territory                     {get;set;}
        public string Territorycode                 {get;set;}
        public string tempList                      {get;set;}
        public string FeedbackStatus                {get;set;}
        //public string OnlineActivity(in mins)       {get;set;}
        public string nooflogins                    {get;set;}
        public string lastCycleCalls                {get;set;}
        public string proposedCalls                 {get;set;}
        public string OrginalCalls                  {get;set;}
        public string lastCycleTarget               {get;set;}
        public string proposedTarget                {get;set;}
        public string originalTarget                {get;set;}
        public Integer add                          {get;set;}
        public Integer drop                         {get;set;}
        public decimal Disruption                   {get;set;}
        public decimal change                       {get;set;}
        public Integer countVal                     {get;set;}  
        
       
         
        public  wrapperclass1(string feedback,Integer countVal){
                this.FeedbackStatus=feedback;
                this.countVal=countVal;
              
        }
        public  wrapperclass1(string changerequest,string changerequestid,string positionid,string Territory , string Territorycode,string FeedbackStatus,string nooflogins,string lastCycleCalls ,string proposedCalls ,string OrginalCalls,string lastCycleTarget,string proposedTarget ,  string originalTarget,Integer add ,Integer drop,decimal Disruption,decimal change){
            
            this.changerequest=changerequest;
            this.changerequestid=changerequestid;
            this.positionid=positionid;
            this.Territory=Territory;
            this.Territorycode=Territorycode;
            this.FeedbackStatus=FeedbackStatus;
            this.nooflogins=nooflogins;
            
            this.lastCycleCalls=String.valueof(Integer.valueof(lastCycleCalls));
            this.proposedCalls=String.valueof(Integer.valueof(proposedCalls));
            this.OrginalCalls=String.valueof(Integer.valueof(OrginalCalls));
            this.lastCycleTarget=lastCycleTarget;
            this.proposedTarget=proposedTarget;
            this.originalTarget=originalTarget;
            this.add=add;
            this.drop=drop;
            this.Disruption=Disruption;
            this.change=change;
            this.tempList='';
            
        }
    }
    public class wrapperclass{
    	
    		public Integer countValue{get;set;}
    		public String range{get;set;} 
    		public string territoryCode{get;set;}
    		public string territoryName{get;set;}
    		
    		
    		public  wrapperclass(Integer countValue, String range,string  territoryCode,string territoryName){
    				
    				this.countValue=countValue;
    				this.range=range;
    				this.territoryName=territoryName;
    				this.territoryCode=territoryCode;
    				
    		}
    	
    	
    	
    	}
	
}
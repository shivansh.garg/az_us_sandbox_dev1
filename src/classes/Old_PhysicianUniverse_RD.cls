/**
 * @author - Sahil Mahajan
 *
 * 
 */
 

public with sharing class Old_PhysicianUniverse_RD {


    Set<Id> pos{get;set;}                                   // All unique positions of Rep
    public List<select_wrap> displayList {get;set;}         // List to be displayed on Page
    List<AxtriaSalesIQTM__User_Access_Permission__c> positions {get;set;}    // All the positions of the Rep
    List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> ListUniverseAcc{get;set;}   // Temporary List for all Records
    string sortField {get;set;}                             
    string sortdir {get;set;}
    string soql {get;set;}  
    public string userType                                   {get;set;}
    
    public Old_PhysicianUniverse_RD()
    {
        // Initialising Variables and Lists
        sortField = 'AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__FirstName__c';
        sortdir = 'asc';
        soql = 'select AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__FirstName__c,AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__LastName__c,AxtriaSalesIQTM__Account__r.Name,AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__Speciality__c,AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__AccountType__c,AxtriaSalesIQTM__Account__r.BillingCity,AxtriaSalesIQTM__Account__r.BillingState,AxtriaSalesIQTM__Account__r.BillingPostalCode,AxtriaSalesIQTM__ReasonDrop__c,AxtriaSalesIQTM__ReasonAdd__c,AxtriaSalesIQTM__Calls_Approved__c,AxtriaSalesIQTM__Calls_Updated__c,AxtriaSalesIQTM__Call_Sequence_Approved__c,AxtriaSalesIQTM__Call_Sequence_Updated__c,AxtriaSalesIQTM__Comments__c,AxtriaSalesIQTM__Rank__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Position__c in :pos and AxtriaSalesIQTM__isIncludedCallPlan__c = False';
        
        loggedInUserData     = new List<AxtriaSalesIQTM__User_Access_Permission__c>();
        loggedInUserData = SalesIQUtility.getUserAccessPermistion(Userinfo.getUserId());
        if(loggedInUserData!=null && loggedInUserData.size()>0){   
            if(loggedInUserData[0].AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Position_Type__c =='District'){
                userType = 'District';
                soql = 'select AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__FirstName__c,AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__LastName__c,AxtriaSalesIQTM__Account__r.Name,AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__Speciality__c,AxtriaSalesIQTM__Account__r.BillingCity,AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__AccountType__c,AxtriaSalesIQTM__Account__r.BillingState,AxtriaSalesIQTM__Account__r.BillingPostalCode,AxtriaSalesIQTM__ReasonDrop__c,AxtriaSalesIQTM__ReasonAdd__c,AxtriaSalesIQTM__Calls_Approved__c,AxtriaSalesIQTM__Calls_Updated__c,AxtriaSalesIQTM__Call_Sequence_Approved__c,AxtriaSalesIQTM__Call_Sequence_Updated__c,AxtriaSalesIQTM__Comments__c,AxtriaSalesIQTM__Rank__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Position__c = \'a0d41000000dU7pAAE\' and AxtriaSalesIQTM__isIncludedCallPlan__c = False';
            }
        }
        
        pos = new Set<Id>();
        positions = [select AxtriaSalesIQTM__Position__c from AxtriaSalesIQTM__User_Access_Permission__c where AxtriaSalesIQTM__User__c = :Userinfo.getUserId()];
 
        for (AxtriaSalesIQTM__User_Access_Permission__c p1 :positions)
        {
            pos.add(p1.AxtriaSalesIQTM__Position__c);
        }
        runquery();         

    }

    // Wrapper Class for displaying Records 
    public class select_wrap
    {
        
       public boolean selected{get;set;}
       public  AxtriaSalesIQTM__Position_Account_Call_Plan__c rec{get;set;}
       
       public select_wrap(AxtriaSalesIQTM__Position_Account_Call_Plan__c rec)
       {
            selected = false;
            this.rec = rec;
            
       }
    }
    
    
    public pagereference redirectToReasonPage()
    {
        //PageReference
        PageReference pg = new PageReference('/apex/ReasonAdd');
        //pg.setRedirect(true);
        return pg;  
    }
    
    List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> pacp;
    
    
    // Function executed when a physician is to be moved from Universe to Call Plan 
    public void IncludeInCallplan()
    {
        pacp = new List<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
        getphyAddDrop(); //Changes -Added
        for(AxtriaSalesIQTM__Position_Account_Call_Plan__c sw:reasonPacp)
        {   
            sw.AxtriaSalesIQTM__isIncludedCallPlan__c=True;
            
            if(sw.AxtriaSalesIQTM__isModified__c==True)  // If originally (after last Approved ) the record was in Call Plan
            {
                sw.AxtriaSalesIQTM__isModified__c = False;
                sw.AxtriaSalesIQTM__Change_Status__c='Original'; 
                sw.AxtriaSalesIQTM__ReasonAdd__c = 'None';           
                
            }
            else
            {
                sw.AxtriaSalesIQTM__isModified__c = True;
                sw.AxtriaSalesIQTM__Change_Status__c='Pending for Submission';
            }
            sw.AxtriaSalesIQTM__ReasonDrop__c = 'None';
            pacp.add(sw);
        }   
        update pacp;
        runquery();
    }
    
    List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> reasonPacp; 
    
    // Function to get Add Reason of Physician
    public List<AxtriaSalesIQTM__Position_Account_Call_Plan__c> getphyAddDrop()
    {
        reasonPacp = new List<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
        system.debug('displayList is '+displayList);
        for(select_wrap sw:displayList)
        {
            if(sw.selected)
            {
                reasonPacp.add(sw.rec);
            }
        }   
        return reasonPacp;
    }
    
    // Function Saves the Add Reason, Adds Physician to Call Plan and then redirect back to Phsyician Universe Page    
    public pagereference saveReason()
    {
        IncludeInCallplan();
        PageReference pg = new PageReference('/apex/CallMax');
        pg.setRedirect(true);
        return pg;
    }
    
    public pagereference cancelReason()
    {
        PageReference pg = new PageReference('/apex/PhysicianUniverse');
        pg.setRedirect(true);
        return pg;
    }
    
    // Function to run SOQL Queries
   public void runquery()
   {
        displayList = new List<select_wrap>();
        ListUniverseAcc = new List<AxtriaSalesIQTM__Position_Account_Call_Plan__c>();
        system.debug('soql is '+soql);
        system.debug('pos is '+pos);
         system.debug('sortDir is '+sortDir);
        
        system.debug('soql-----'+soql);
        
        ListUniverseAcc = Database.query(soql+' order by '+ sortField + ' ' + sortDir + ' NULLS LAST');
        
        for(AxtriaSalesIQTM__Position_Account_Call_Plan__c c1 : ListUniverseAcc)
        {
            displayList.add(new select_wrap(c1));
        }
   }
   
   public void toggleSort() 
   {
        sortDir = sortDir.equals('asc') ? 'desc' : 'asc';
        runquery();
   }
   
       
       public List<AxtriaSalesIQTM__User_Access_Permission__c> loggedInUserData  {get;set;}
       
       // Executed when Search is Called
       public void  runSearch()
        {  
           displayList = new List<select_wrap>();
           
           //Fetching parameters from Page  
           String ids = Apexpages.currentPage().getParameters().get('id');
           String firstName = Apexpages.currentPage().getParameters().get('firstname');
            String city= Apexpages.currentPage().getParameters().get('city');       
            String state= Apexpages.currentPage().getParameters().get('state');
            String specialty= Apexpages.currentPage().getParameters().get('specialty');
        
                soql = 'select AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__FirstName__c,AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__LastName__c,AxtriaSalesIQTM__Account__r.Name,AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__Speciality__c,AxtriaSalesIQTM__Account__r.BillingCity,AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__AccountType__c,AxtriaSalesIQTM__Account__r.BillingState,AxtriaSalesIQTM__Account__r.BillingPostalCode,AxtriaSalesIQTM__ReasonDrop__c,AxtriaSalesIQTM__ReasonAdd__c,AxtriaSalesIQTM__Calls_Approved__c,AxtriaSalesIQTM__Calls_Updated__c,AxtriaSalesIQTM__Call_Sequence_Approved__c,AxtriaSalesIQTM__Call_Sequence_Updated__c,AxtriaSalesIQTM__Comments__c,AxtriaSalesIQTM__Rank__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Position__c in :pos and AxtriaSalesIQTM__isIncludedCallPlan__c = False';
        
        
            loggedInUserData     = new List<AxtriaSalesIQTM__User_Access_Permission__c>();
            loggedInUserData = SalesIQUtility.getUserAccessPermistion(Userinfo.getUserId());
            if(loggedInUserData!=null && loggedInUserData.size()>0)
            {
                if(loggedInUserData[0].AxtriaSalesIQTM__Position__r.AxtriaSalesIQTM__Position_Type__c =='District'){
                soql = 'select AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__FirstName__c,AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__LastName__c,AxtriaSalesIQTM__Account__r.Name,AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__Speciality__c,AxtriaSalesIQTM__Account__r.BillingCity,AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__AccountType__c,AxtriaSalesIQTM__Account__r.BillingState,AxtriaSalesIQTM__Account__r.BillingPostalCode,AxtriaSalesIQTM__ReasonDrop__c,AxtriaSalesIQTM__ReasonAdd__c,AxtriaSalesIQTM__Calls_Approved__c,AxtriaSalesIQTM__Calls_Updated__c,AxtriaSalesIQTM__Call_Sequence_Approved__c,AxtriaSalesIQTM__Call_Sequence_Updated__c,AxtriaSalesIQTM__Comments__c,AxtriaSalesIQTM__Rank__c from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__Position__c = \'a0d41000000dU7pAAE\' and AxtriaSalesIQTM__isIncludedCallPlan__c = False';
                }
            }
        
            if (ids != null && !ids.equals(''))
                 soql += ' and AxtriaSalesIQTM__Account__r.AccountNumber LIKE \''+String.escapeSingleQuotes(ids)+'%\'  ';
                 
            if (firstname != null && !firstName.equals(''))
                 soql += ' and AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__FirstName__c LIKE \''+String.escapeSingleQuotes(firstName)+'%\'  ';

            if (city!= null && !city.equals(''))
                 soql += ' and AxtriaSalesIQTM__Account__r.BillingCity LIKE \''+String.escapeSingleQuotes(city)+'%\'  ';
                 
            if (state!= null && !state.equals(''))
                soql += ' and AxtriaSalesIQTM__Account__r.BillingState LIKE \''+String.escapeSingleQuotes(state)+'%\'   ';   
            if (specialty!= null && !specialty.equals(''))
                soql += ' and AxtriaSalesIQTM__Account__r.AxtriaSalesIQTM__Speciality__c LIKE \''+String.escapeSingleQuotes(specialty)+'%\'   ';     
            
            
            
           runQuery(); 
        }
        
    public String searchString{get; set;}
    public String city{get; set;}
    public String state{get; set;}
    public String specialty{get; set;}
    public String ids{get; set;}
    public void clearSearch()
    {
        searchString='';
        city='';
        ids='';
        state='';
        specialty='';
        sortField ='AxtriaSalesIQTM__Account__r.name';
       runSearch();
    }
}
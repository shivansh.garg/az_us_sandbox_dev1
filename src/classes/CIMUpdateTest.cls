@istest
public with sharing class CIMUpdateTest {
    
     static testMethod void cimtest(){
     
        User loggedInUser = [select id,userrole.name from User where id=:UserInfo.getUserId()];
        
        System.RunAs(loggedInUser){
        
            AxtriaSalesIQTM__Team__c team = new AxtriaSalesIQTM__Team__c();
            team.Name = 'Hospital-Q2-2016';
            insert team;
            
            AxtriaSalesIQTM__Team_Instance__c teamIns = new AxtriaSalesIQTM__Team_Instance__c();
            teamIns.AxtriaSalesIQTM__Team__c = team.id;
            insert teamIns;
            
            AxtriaSalesIQTM__Position__c pos = new AxtriaSalesIQTM__Position__c();
            pos.AxtriaSalesIQTM__Position_Type__c = 'Territory';
            pos.Name = 'Chico CA_SPEC';
            pos.AxtriaSalesIQTM__Team_iD__c  = team.id;
            
            insert pos;
            
            AxtriaSalesIQTM__Position__c pos1 = new AxtriaSalesIQTM__Position__c();
            pos1.AxtriaSalesIQTM__Position_Type__c = 'District';
            pos1.Name = 'Chico CA_SPEC1';
            pos1.AxtriaSalesIQTM__Team_iD__c     = team.id;
            
            insert pos1;
            
            AxtriaSalesIQTM__Position_Team_Instance__c posTeam = new AxtriaSalesIQTM__Position_Team_Instance__c();
            posTeam.AxtriaSalesIQTM__Position_ID__c = pos.id;
            posTeam.AxtriaSalesIQTM__Parent_Position_ID__c = pos.id;
            posTeam.AxtriaSalesIQTM__Team_Instance_ID__c = teamIns.id;
            posTeam.AxtriaSalesIQTM__Effective_End_Date__c = Date.newInstance(2022,11,09);
            posTeam.AxtriaSalesIQTM__Effective_Start_Date__c = Date.newInstance(2016,02,29);

            
            insert posTeam;
            
            AxtriaSalesIQTM__Position_Team_Instance__c posTeam1 = new AxtriaSalesIQTM__Position_Team_Instance__c();
            posTeam1.AxtriaSalesIQTM__Position_ID__c = pos1.id;
            posTeam1.AxtriaSalesIQTM__Parent_Position_ID__c = pos1.id;
            posTeam1.AxtriaSalesIQTM__Team_Instance_ID__c = teamIns.id;
            posTeam1.AxtriaSalesIQTM__Effective_End_Date__c = Date.newInstance(2022,11,09);
            posTeam1.AxtriaSalesIQTM__Effective_Start_Date__c = Date.newInstance(2016,02,29);

            
            insert posTeam1;
            
            Account acc = new Account();
            acc.Name = 'Chelsea Parson';
            insert acc;
            
            AxtriaSalesIQTM__Position_Account_Call_Plan__c posAcc = new AxtriaSalesIQTM__Position_Account_Call_Plan__c();
            posAcc.AxtriaSalesIQTM__Position__c = pos.id;
            posAcc.AxtriaSalesIQTM__Position_Team_Instance__c = posTeam.id;
            posAcc.AxtriaSalesIQTM__Team_Instance__c = teamIns.id;
            posAcc.AxtriaSalesIQTM__Account__c = acc.id;
            posAcc.AxtriaSalesIQTM__Effective_End_Date__c = Date.newInstance(2016,08,09);
            posAcc.AxtriaSalesIQTM__Effective_Start_Date__c = Date.newInstance(2016,04,04);
            posAcc.AxtriaSalesIQTM__Metric1__c = 10;
            posAcc.AxtriaSalesIQTM__isAccountTarget__c=True;
            insert posAcc;
            
            AxtriaSalesIQTM__CIM_Config__c cim = new AxtriaSalesIQTM__CIM_Config__c();
            cim.AxtriaSalesIQTM__Aggregation_Attribute_API_Name__c = 'AxtriaSalesIQTM__Position_Team_Instance__c';
            cim.AxtriaSalesIQTM__Aggregation_Object_Name__c = 'AxtriaSalesIQTM__Position_Account_Call_Plan__c';
            cim.AxtriaSalesIQTM__Object_Name__c = 'AxtriaSalesIQTM__Position_Account_Call_Plan__c';
            cim.AxtriaSalesIQTM__team_instance__c = teamIns.id;
            cim.AxtriaSalesIQTM__Aggregation_Type__c = 'Sum';   
            cim.AxtriaSalesIQTM__Aggregation_Condition_Attribute_API_Name__c = 'AxtriaSalesIQTM__isAccountTarget__c = true';
            cim.AxtriaSalesIQTM__Aggregation_Condition_Attribute_Value__c = 'True';
            cim.AxtriaSalesIQTM__Attribute_API_Name__c = 'AxtriaSalesIQTM__Metric1__c';
            insert cim;
            
            
            AxtriaSalesIQTM__CIM_Config__c cim2 = new AxtriaSalesIQTM__CIM_Config__c();
            cim2.AxtriaSalesIQTM__Aggregation_Attribute_API_Name__c = 'AxtriaSalesIQTM__Position_Team_Instance__c';
            cim2.AxtriaSalesIQTM__Aggregation_Object_Name__c = 'AxtriaSalesIQTM__Position_Account_Call_Plan__c';
            cim2.AxtriaSalesIQTM__Object_Name__c = 'AxtriaSalesIQTM__Position_Account_Call_Plan__c';
            cim2.AxtriaSalesIQTM__team_instance__c = teamIns.id;
            cim2.AxtriaSalesIQTM__Aggregation_Type__c = 'Sum';  
            //cim.AxtriaSalesIQTM__Aggregation_Condition_Attribute_API_Name__c = 'true';
            //cim.AxtriaSalesIQTM__Aggregation_Condition_Attribute_Value__c = 'True';
            cim2.AxtriaSalesIQTM__Attribute_API_Name__c = 'AxtriaSalesIQTM__Metric1__c';
            insert cim2;
             
            AxtriaSalesIQTM__CIM_Position_Metric_Summary__c cimSummary = new AxtriaSalesIQTM__CIM_Position_Metric_Summary__c();
            cimSummary.AxtriaSalesIQTM__CIM_Config__c = cim.id;
            
            insert cimSummary;
            
            CIMPositionMatrixSummaryUpdateCtlr cimMetric = new CIMPositionMatrixSummaryUpdateCtlr();
            
            cimMetric.cimConfigId = cim.id;
            cimMetric.teamInstanceID = teamIns.id;
            
            cimMetric.displayQuery();
            
            
            cimMetric.cimConfigId = cim2.id;
            cimMetric.teamInstanceID = teamIns.id;
            
            cimMetric.displayQuery();

            AxtriaSalesIQTM__CIM_Config__c cim1 = new AxtriaSalesIQTM__CIM_Config__c();
            cim1.AxtriaSalesIQTM__Aggregation_Attribute_API_Name__c = 'AxtriaSalesIQTM__Position_Team_Instance__c';
            cim1.AxtriaSalesIQTM__Aggregation_Object_Name__c = 'AxtriaSalesIQTM__Position_Account_Call_Plan__c';
            cim1.AxtriaSalesIQTM__Object_Name__c = 'AxtriaSalesIQTM__Account__c';
            cim1.AxtriaSalesIQTM__team_instance__c = teamIns.id;
            cim1.AxtriaSalesIQTM__Aggregation_Type__c = 'Count';    
            cim1.AxtriaSalesIQTM__Aggregation_Condition_Attribute_API_Name__c = 'AxtriaSalesIQTM__isAccountTarget__c = true';
            cim1.AxtriaSalesIQTM__Aggregation_Condition_Attribute_Value__c = 'TRUE';
            cim1.AxtriaSalesIQTM__Attribute_API_Name__c = 'id';
            insert cim1;
            
            CIMPositionMatrixSummaryUpdateCtlr cimMetric1 = new CIMPositionMatrixSummaryUpdateCtlr();
            cimMetric1.cimConfigId = cim1.id;
            cimMetric1.teamInstanceID = teamIns.id;
            
            cimMetric1.displayQuery();
            cimMetric.updatePostionMatrxiSummary();
            
            
            AxtriaSalesIQTM__CIM_Config__c cim3 = new AxtriaSalesIQTM__CIM_Config__c();
            cim3.AxtriaSalesIQTM__Aggregation_Attribute_API_Name__c = 'AxtriaSalesIQTM__Position_Team_Instance__c';
            cim3.AxtriaSalesIQTM__Aggregation_Object_Name__c = 'AxtriaSalesIQTM__Position_Account_Call_Plan__c';
            cim3.AxtriaSalesIQTM__Object_Name__c = 'AxtriaSalesIQTM__Account__c';
            cim3.AxtriaSalesIQTM__team_instance__c = teamIns.id;
            cim3.AxtriaSalesIQTM__Aggregation_Type__c = 'Count';    
            //cim3.AxtriaSalesIQTM__Aggregation_Condition_Attribute_API_Name__c = 'AxtriaSalesIQTM__isAccountTarget__c';
            //cim3.AxtriaSalesIQTM__Aggregation_Condition_Attribute_Value__c = 'TRUE';
            cim3.AxtriaSalesIQTM__Attribute_API_Name__c = 'id';
            insert cim3;
            
            cimMetric1.cimConfigId = cim3.id;
            cimMetric1.teamInstanceID = teamIns.id;
            
            cimMetric1.displayQuery();
            cimMetric.updatePostionMatrxiSummary();
        
        
        }
        
    }
    
}
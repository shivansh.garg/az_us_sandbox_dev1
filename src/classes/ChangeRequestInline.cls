public with sharing class ChangeRequestInline{
    private ApexPages.StandardController ctrl{get; set;}
    private AxtriaSalesIQTM__Change_Request__c changeRequest;
    public Integer maxTotalHCPs {get;set;}
   public Integer maxnexavarCalls {get;set;}
   
   public Integer maxstivargaCalls {get;set;}
   public Integer maxtotalCallsHCP {get;set;}
    
     
   public Integer percentageTotalHcps {get;set;}
   public Integer percentagenexavarCalls {get;set;}
   public Integer percentagestivargaCalls {get;set;}
   public Integer percentagetotalCallsHCP {get;set;}
   
   public integer phyAdded {get;set;}
   public integer phyDropped {get;set;}
   
   
   public string colorNexavarCalls {get;set;}
   public string colorTotalHCPs {get;set;}
   public string colorStivargaCalls {get;set;}
   public string colortotalCallsHCP {get;set;}
   public boolean lock {get;set;}  
   public boolean savelock {get;set;}
   public string selectedPosition {get;set;} 
   public string selectedTeamInstance {get;set;} 
   
   public Integer totalCallsHCP {get;set;}
	public Integer totalHCPs {get;set;}
	public Integer originaltotalCallsHCP {get;set;}
	public Integer originaltotalHCPs {get;set;}
	public Integer nexavarCalls {get;set;}
	public Integer stivargaCalls {get;set;} 
	//public string genericCssForBar {get;set;}
	public string genericCssForBarCalls {get;set;}
	public string genericCssForBarTarget {get;set;}
    
    public ChangeRequestInline(ApexPages.StandardController ctrl){
        lock = false;
        this.ctrl = ctrl;
        this.changeRequest = (AxtriaSalesIQTM__Change_Request__c)ctrl.getRecord();
        AxtriaSalesIQTM__Change_Request__c obj = [select id, AxtriaSalesIQTM__destination_position__c, AxtriaSalesIQTM__Team_Instance_ID__c from AxtriaSalesIQTM__change_request__c where id=: this.changeRequest.id];
        selectedPosition = obj.AxtriaSalesIQTM__destination_position__c;
        selectedTeamInstance = obj.AxtriaSalesIQTM__Team_Instance_ID__c;
        //genericCssForBar = 'left,#FF0000 ' + 12.50 + '%,#ffbf00 ' + 25.00 + '%,#00FF00 ' + 75.00 + '%,#ffbf00 ' + 87.50 + '%,#FF0000 100%';
        //genericCssForBarCalls = 'left,#FF0000 '+25.00+'%,#00FF00 '+75.00+'%';
        genericCssForBarCalls = 'left,#FF0000 30.00%,#00FF00 50.00%, #ffbf00 60%';
        genericCssForBarTarget = 'left,#ffbf00 '+40.00+'%,#00FF00 '+45.00+'%,#00FF00 '+55.00+'%,#ffbf00 '+60.00+'%,#ffbf00 '+100.00+'%';
        
        
        cimConfig();
    }
    
    
     public void cimConfig()
   {    
        
        maxTotalHCPs = 0;
            maxtotalCallsHCP = 0;
            
            AggregateResult aggResultOriginal = [select count(id) targets, sum(Original_Calls_Numeric__c) calls from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__isAccountTarget__c = true and AxtriaSalesIQTM__Position__c = :selectedPosition and AxtriaSalesIQTM__Team_Instance__c = :selectedTeamInstance];
            AggregateResult aggResultUpdated  = [select count(id) targets, sum(Calls_Numeric__c) calls from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__isIncludedCallPlan__c = true and AxtriaSalesIQTM__Position__c =:selectedPosition and AxtriaSalesIQTM__Team_Instance__c =:selectedTeamInstance ];
            
            Integer originalTargets = Integer.valueof(aggResultOriginal.get('targets'));
            Integer originalCalls   = Integer.valueof(aggResultOriginal.get('calls'));
            
            Integer updatedTargets = Integer.valueof(aggResultUpdated.get('targets'));
            Integer updatedCalls   = Integer.valueof(aggResultUpdated.get('calls'));
            
            Decimal originalCallsDecimal = Decimal.valueof(originalCalls);
            Decimal updatedCallsDecimal = Decimal.valueof(updatedCalls);
            
            Decimal originalTargetsDecimal = Decimal.valueof(originalTargets);
            Decimal updatedTargetsDecimal = Decimal.valueof(updatedTargets);
            
            totalHCPs = updatedTargets;
            originaltotalHCPs = originalTargets;
            
            totalCallsHCP = updatedCalls;
            originaltotalCallsHCP = originalCalls;
            
            if(originaltotalHCPs == null || originaltotalCallsHCP == null)
            {
            maxTotalHCPs = 0;
            maxtotalCallsHCP = 0;
                
            }
            else
            {
                maxTotalHCPs = originaltotalHCPs * 2;
                maxtotalCallsHCP = originaltotalCallsHCP * 2;
    
            }
            if(originalTargets != 0)
            {
               percentageTotalHcps = Integer.valueof((updatedTargetsDecimal * 100)/originalTargetsDecimal); 
            }
            else
            {
                percentageTotalHcps = 0;
            }
            
             if( (percentageTotalHcps < 90) || (percentageTotalHcps > 110 ))
             {
                colorTotalHCPs = 'yellow';  
               
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Warning, 'Your physician count has changed by more than +/-10% of your original physician count.'));
            }
            else if(percentageTotalHcps >= 90 && percentageTotalHcps <= 110){
                colorTotalHCPs = 'green';
            }
    
            if(originalCalls != 0 && originalCalls !=null)
            {
                percentagetotalCallsHCP = Integer.valueof((updatedCallsDecimal * 100)/originalCallsDecimal); 
            }
            
            else
            {
                percentagetotalCallsHCP = 0;
            }
            
            
            // Checking Calls Metrics
            if(updatedCalls < 1800)
            {
               colortotalCallsHCP = 'red';
               savelock = true; 
               ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'The Total Calls in your Call Plan are less than 1800. You’ll be able to submit your Call Plan only after the Total Calls are greater than 1800.'));
            }
            else if(percentagetotalCallsHCP>=110){
               colortotalCallsHCP = 'yellow';
           
               ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Warning, 'The Total Calls in your Call Plan are more than 10% of your original Calls'));
            }
            else
            {
                colortotalCallsHCP ='green';
            }
            
            maxtotalCallsHCP = 3600;
        
            system.debug('+++++++++++++ In cim selected Position and team instance is '+selectedPosition+' '+selectedTeamInstance);
            AggregateResult aggAdded = [select count(id) countRecs from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__isAccountTarget__c = false and AxtriaSalesIQTM__isIncludedCallPlan__c = true and AxtriaSalesIQTM__Position__c = :selectedPosition and AxtriaSalesIQTM__Team_Instance__c = :selectedTeamInstance];
            phyAdded = Integer.valueof(aggAdded.get('countRecs'));              
            
            AggregateResult aggDropped = [select count(id) countRecs from AxtriaSalesIQTM__Position_Account_Call_Plan__c where AxtriaSalesIQTM__isAccountTarget__c = true and AxtriaSalesIQTM__isIncludedCallPlan__c = false and AxtriaSalesIQTM__Position__c = :selectedPosition and AxtriaSalesIQTM__Team_Instance__c = :selectedTeamInstance];
            phyDropped = Integer.valueof(aggDropped.get('countRecs'));
            
            system.debug('+++++++++++++++++++ phy Added and Phy Dropped '+ phyAdded + ' '+aggDropped);
           
            
    } 
}